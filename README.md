# terraform.builder
This project builds a docker image with basic tools for Terraform Infrastructure.

## Usage
Ex. Inside your .gitlab-ci.yml file make the following changes:
```
image: registry.gitlab.com/itomychstudio/builders/terraform.builder:latest

stages:
  - build

build:
  stage: build
  before_script:
    - terraform --version
    - aws --version
    - az --version
    - kubectl version
  script:
    - echo "Hello World!"
```

## Tools
  - bash
  - git
  - curl
  - pip (based on python3)
  - kubectl
  - aws-cli v2 2.2.14
  - azure cli 2.30.0
  - terraform cli 1.1.2

## Contributing
Create branch with the 'feature/ticket-name' name. Make changes and commit.
Then create the merge request into 'develop' branch.

***
## Authors and acknowledgment
ITOMYCH STUDIO/DashDevs LLC