FROM alpine:3.14

ENV TERRAFORM_VERSION 1.1.2

RUN apk add --no-cache curl bash git
RUN apk add --no-cache python3 py3-pip
RUN apk add gcc musl-dev python3-dev libffi-dev openssl-dev cargo make
    
    # Install Azure CLI
RUN pip install azure-cli \
    # Install AWS_CLI_v2
    && curl -sL https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip -o awscliv2.zip \
    && unzip awscliv2.zip \
    && aws/install \
    && rm -rf \
        awscliv2.zip \
        /usr/local/aws-cli/v2/*/dist/aws_completer \
        /usr/local/aws-cli/v2/*/dist/awscli/data/ac.index \
        /usr/local/aws-cli/v2/*/dist/awscli/examples \
        /tmp/* \
        /var/cache/apk/*

# Install Kubernetes CLI
RUN curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl" \
    && install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl

# Install terraform cli
RUN curl https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_linux_amd64.zip -o terraform_cli.zip \
    && unzip terraform_cli.zip -d /usr/local/bin \
    && rm -rf terraform_cli.zip
